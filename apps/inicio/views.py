from os.path import join

from django.shortcuts import render
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required(login_url='usuarios:login')
def inicio(request):
    template = join("default", "index.html")
    return render(request, template_name=template, context={
    })