from django import forms



class SearchForm(forms.Form):
    search_field = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': 'Que estas buscando hoy?',
               "class":"form-control form-control-lg",
               }
    ), required=False)
    categories = forms.ChoiceField(widget=forms.Select(
        attrs={
            'class': 'form-control form-control-lg',
        }
    ), required=False)