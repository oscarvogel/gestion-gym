

def paramsist_processors(request):

    whatsapp ="+54 9 374 340-0209"
    whatsapp_link = whatsapp.replace("+","").replace("-","").replace(" ", "")
    url_consulta_wsp = "https://wa.me/{}?text=Hola%20desearia%20saber%20el%20precio%20de%20".format(
        whatsapp_link
    )

    return {
        'whatsapp': whatsapp,
        'whatsapp_link': whatsapp_link,
        'url_consulta_wsp': url_consulta_wsp
    }