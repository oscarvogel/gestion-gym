from datetime import datetime

from django import forms

from apps.alumnos.models import Alumno, Pagos


class AlumnoForm(forms.ModelForm):

    class Meta:
        model = Alumno
        exclude = ['id','empresa']
        widgets = {
            'fecha_nacimiento': forms.DateInput(attrs={
                'class':'form-control datepicker',
            })
        }
        labels = {
            "fecha_nacimiento": "Fecha de ingreso"
        }

class PagoForm(forms.ModelForm):

    class Meta:
        model = Pagos
        exclude = ['id', 'alumno']

class BusquedaAlumnoForm(forms.Form):
    nombre = forms.CharField(widget=forms.TextInput(
        attrs={
            'placeholder':'Busqueda'
        }
    ))

class DesdeHastaFecha(forms.Form):
    # desde = forms.DateField(initial=datetime.now().date())
    # hasta = forms.DateField(initial=datetime.now().date())
    desde = forms.DateField(widget=forms.DateInput(format=('%d/%m/%Y'),
        attrs={'class':'form-control datepicker',}
    ), initial=datetime.now().date())
    hasta = forms.DateField(widget=forms.DateInput(format=('%d/%m/%Y'),
        attrs={'class':'form-control datepicker', }
    ), initial=datetime.now().date())