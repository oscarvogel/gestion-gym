from os.path import join

from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from django.urls import reverse

from apps.alumnos.forms import AlumnoForm, PagoForm, BusquedaAlumnoForm, DesdeHastaFecha
from apps.alumnos.models import Alumno, Pagos, Clases


@login_required(login_url='usuarios:login')
def listar(request):
    template = join('default', 'alumnos', 'listar.html')
    page = request.GET.get('page', 1)

    form = BusquedaAlumnoForm()
    alumnos_list = Alumno.objects.filter(empresa=request.user)
    if request.method == 'POST':
        form = BusquedaAlumnoForm(request.POST)
        if form.is_valid():
            alumnos_list = alumnos_list.filter(nombre__icontains = form.cleaned_data.get('nombre'))

    paginator = Paginator(alumnos_list, 12)
    try:
        alumnos = paginator.page(page)
    except PageNotAnInteger:
        alumnos = paginator.page(1)
    except EmptyPage:
        alumnos = paginator.page(paginator.num_pages)

    return render(request, template, context={
        'alumnos': alumnos,
        'form': form,
    })

@login_required(login_url='usuarios:login')
def agregar_alumno(request):
    template = join('default', 'alumnos', 'alumno.html')
    if request.method == 'POST':
        form = AlumnoForm(request.POST)
        if form.is_valid():
            nuevo_alumno = form.save(commit=False)
            nuevo_alumno.empresa = request.user
            nuevo_alumno.save()
            return HttpResponseRedirect(reverse('alumnos:listar'))
    else:
        form = AlumnoForm()
        form.fields['clases'].queryset = Clases.objects.filter(empresa=request.user)

    return render(request, template, context={
        'form': form
    })

@login_required(login_url='usuarios:login')
def editar_alumno(request, id=1):
    template = join('default', 'alumnos', 'alumno.html')
    nombre_boton = 'Modificar Alumno'
    form = None
    alumno = None

    if request.method == 'POST':
        form = AlumnoForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            alumno = Alumno.objects.get(id=id, empresa=request.user)
            alumno.nombre = cd['nombre']
            alumno.celular = cd['celular']
            alumno.email = cd['email']
            alumno.fecha_nacimiento = cd['fecha_nacimiento']
            alumno.clases.set(cd['clases'])
            alumno.save()
            return HttpResponseRedirect(reverse('alumnos:listar'))
    else:
        try:
            alumno = Alumno.objects.get(id=id)
            form = AlumnoForm(initial={
                'nombre':alumno.nombre,
                'celular':alumno.celular,
                'email':alumno.email,
                'fecha_nacimiento':alumno.fecha_nacimiento,
                'clases':[c.id for c in alumno.clases.all()]
            })
            form.fields['clases'].queryset = Clases.objects.filter(empresa=request.user)

        except:
            form = None
            alumno = None

    return render(request, template, context={
        'form': form,
        'nombre_boton': nombre_boton
    })

@login_required(login_url='usuarios:login')
def borrar_alumno(request, id=1):
    template = join('default', 'alumnos', 'alumno.html')
    nombre_boton = 'Eliminar Alumno'
    form = None
    alumno = None

    if request.method == 'POST':
        form = AlumnoForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            alumno = Alumno.objects.get(id=id, empresa=request.user)
            alumno.delete()
            return HttpResponseRedirect(reverse('alumnos:listar'))
    else:
        try:
            alumno = Alumno.objects.get(id=id)
            form = AlumnoForm(initial={
                'nombre': alumno.nombre,
                'celular': alumno.celular,
                'email': alumno.email,
                'fecha_nacimiento': alumno.fecha_nacimiento
            })
            print(alumno)
        except:
            form = None
            alumno = None

    return render(request, template, context={
        'form': form,
        'nombre_boton': nombre_boton
    })

@login_required(login_url='usuarios:login')
def registra_pago(request, id=1):

    template = join('default', 'alumnos', 'pagos.html')
    try:
        pagos = Pagos.objects.filter(alumno = id, alumno__empresa = request.user).order_by('-id')[0]
        ultimo_pago = pagos.monto
    except:
        ultimo_pago = 0

    form = PagoForm(initial={
        'monto':ultimo_pago
    })
    alumno = Alumno.objects.get(id=id)
    if request.method == 'POST':
        form = PagoForm(request.POST)
        if form.is_valid():
            pago = form.save(commit=False)
            pago.alumno = alumno
            pago.save()
            return HttpResponseRedirect(reverse('alumnos:listar'))

    return render(request, template, context={
        'form':form,
        'alumno':alumno
    })

@login_required(login_url='usuarios:login')
def lista_pago(request):
    template = join('default', 'alumnos', 'lista_pagos.html')
    form = DesdeHastaFecha()
    pagos = None
    if request.method == 'POST':
        form = DesdeHastaFecha(request.POST)
        if form.is_valid():
            pagos = Pagos.objects.filter(
                fecha__gte = form.cleaned_data.get('desde'),
                fecha__lte = form.cleaned_data.get('hasta'),
                alumno__empresa = request.user
            )
    return render(request, template, context={
        'form': form,
        'pagos': pagos
    })

@login_required(login_url='usuarios:login')
def clases(request):
    template = join('default', 'alumnos', 'clases.html')
    #hago toda la logica en el html con vue js
    return render(request, template)

@login_required(login_url='usuarios:login')
def listar_pagos_alumnos(request, idalumno=0):
    template = join('default', 'alumnos', 'lista_pagos.html')
    form = DesdeHastaFecha()
    pagos = None
    if request.method == 'POST':
        form = DesdeHastaFecha(request.POST)
        if form.is_valid():
            pagos = Pagos.objects.filter(
                fecha__gte = form.cleaned_data.get('desde'),
                fecha__lte = form.cleaned_data.get('hasta'),
                alumno__empresa = request.user,
                alumno__id = idalumno,
            )
    return render(request, template, context={
        'form': form,
        'pagos': pagos
    })