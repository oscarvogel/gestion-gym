# Generated by Django 3.0.8 on 2020-07-04 14:14

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alumnos', '0002_alumno_empresa'),
    ]

    operations = [
        migrations.AddField(
            model_name='alumno',
            name='fecha_nacimiento',
            field=models.DateField(default=datetime.date(2020, 7, 4)),
        ),
    ]
