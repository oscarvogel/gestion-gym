from os.path import join
from django.urls import path
from django.views.generic import TemplateView

from apps.alumnos import views

app_name = 'alumnos'

urlpatterns = [
    path('', views.listar, name='listar'),
    path('agregar', views.agregar_alumno, name='agregar'),
    path('editar/<int:id>', views.editar_alumno, name='editar'),
    path('borrar/<int:id>', views.borrar_alumno, name='borrar'),
    path('registra_pago/<int:id>', views.registra_pago, name='registra_pago'),
    path('lista_pagos', views.lista_pago, name='lista_pago'),
    path('clases', views.clases, name='clases'),
    path('listar_pagos_alumnos/<int:idalumno>', views.listar_pagos_alumnos, name='listar_pagos_alumnos'),
]