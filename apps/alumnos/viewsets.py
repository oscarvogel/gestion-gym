from rest_framework import viewsets, filters

from apps.alumnos.models import Clases, Pagos
from apps.alumnos.serializers import ClasesSerializer, PagosSerializer


class ClasesViewSet(viewsets.ModelViewSet):
    serializer_class = ClasesSerializer
    queryset = Clases.objects.filter()

    filter_backends = (filters.SearchFilter,)
    search_fields = ['nombre']

    def get_queryset(self):
        user = self.request.user
        return Clases.objects.filter(empresa=user.id)


class PagosViewSet(viewsets.ModelViewSet):
    serializer_class = PagosSerializer
    queryset = Pagos.objects.filter()

    def get_queryset(self):
        user = self.request.user
        return Pagos.objects.filter(alumno__empresa=user.id)
