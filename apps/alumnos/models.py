from datetime import datetime

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Clases(models.Model):
    nombre = models.CharField(max_length=80, default='')
    monto = models.DecimalField(max_digits=12, decimal_places=2)
    empresa = models.ForeignKey(User, on_delete=models.DO_NOTHING, default=1)

    class Meta:
        ordering = ['nombre']
        verbose_name = 'Clase'
        verbose_name_plural = 'Clases'

    def __str__(self):
        return self.nombre

class Alumno(models.Model):

    nombre = models.CharField(max_length=50, default='')
    celular = models.CharField(max_length=50, default='', blank=True)
    email = models.EmailField(max_length=150, default='', blank=True)
    empresa = models.ForeignKey(User, on_delete=models.DO_NOTHING, default=1)
    fecha_nacimiento = models.DateField(default=datetime.now().date())
    clases = models.ManyToManyField(Clases, default=1)

    def __str__(self):
        return self.nombre


class Pagos(models.Model):

    alumno = models.ForeignKey(Alumno, on_delete=models.DO_NOTHING)
    fecha = models.DateField(default=datetime.now().date())
    monto = models.DecimalField(max_digits=12, decimal_places=2, default=0)
    comentario = models.CharField(max_length=150, default='')

