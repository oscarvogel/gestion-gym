from rest_framework import routers

from apps.alumnos import viewsets

router = routers.SimpleRouter()
router.register('clases', viewsets.ClasesViewSet)
router.register('pagos', viewsets.PagosViewSet)

urlpatterns = router.urls