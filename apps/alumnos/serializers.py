from rest_framework import serializers

from apps.alumnos.models import Clases, Pagos


class ClasesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Clases
        fields = '__all__'

class PagosSerializer(serializers.ModelSerializer):

    class Meta:
        model = Pagos
        fields = '__all__'