from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class SignUpForm(UserCreationForm):
    username = forms.CharField(max_length=30, label='Nombre usuario',
                                 widget=forms.TextInput(
                                     attrs={
                                         'class':'form-control input-lg',
                                         'placeholder':'Nombre Usuario'
                                     }
                                 ))
    first_name = forms.CharField(max_length=30, required=False, help_text='Opcional.', label='Nombre',
                                 widget=forms.TextInput(
                                     attrs={
                                         'class':'form-control input-lg',
                                         'placeholder':'Nombre'
                                     }
                                 ))
    last_name = forms.CharField(max_length=30, required=False, help_text='Opcional.', label='Apellido',
                                 widget=forms.TextInput(
                                     attrs={
                                         'class':'form-control input-lg',
                                         'placeholder':'Apellido'
                                     }
                                 ))
    email = forms.EmailField(max_length=254, help_text='Requerido. Informe un correo electronico valido.',
                                 widget=forms.EmailInput(
                                     attrs={
                                         'class':'form-control input-lg',
                                         'placeholder':'Correo electronico'
                                     }
                                 ))
    celular = forms.CharField(max_length=30, required=False, help_text='Opcional.', label='Numero de celular',
                                 widget=forms.TextInput(
                                     attrs={
                                         'class':'form-control input-lg',
                                         'placeholder':'Numero de celular'
                                     }
                                 ))
    password1 = forms.CharField(
        label= "Contraseña",
        strip = False,
        widget = forms.PasswordInput(attrs={
            'autocomplete': 'new-password',
            'class':'form-control input-lg',
            'placeholder':'Contraseña'
        }),
        help_text = password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label = ("Confirmacion Contraseña"),
        widget = forms.PasswordInput(attrs={
            'autocomplete': 'new-password',
            'class':'form-control input-lg',
            'placeholder': 'Repita Contraseña'
        }),
        strip = False,
        help_text = ("Ingrese la misma contraseña para su verficiacion."),
    )

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'celular', 'password1', 'password2', )
