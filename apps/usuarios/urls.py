from django.urls import path

from apps.usuarios import views

app_name = 'usuarios'

urlpatterns = [
    path('', views.login_propio, name='login'),
    path('logout/', views.user_logout, name='logout'),
    path('registrate/', views.registrate, name='registrate'),
    path('account_activation_sent/', views.account_activation_sent, name='account_activation_sent'),
    path('activate/(<uidb64>[0-9A-Za-z_\-]+)/(<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/',
        views.activate, name='activate'),
]