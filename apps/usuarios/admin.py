from django.contrib import admin

# Register your models here.
from apps.usuarios.models import Perfil


@admin.register(Perfil)
class PerfilAdmin(admin.ModelAdmin):
    list_display = ['user', 'celular']
    list_per_page = 20
    search_fields = ['user__first_name', 'user__last_name']
    list_editable = ['celular']