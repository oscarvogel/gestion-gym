# coding=utf-8
from os.path import join

from django.contrib import messages
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib.auth import login as do_login

# Create your views here.
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode

from apps.usuarios.forms import SignUpForm
from apps.usuarios.tokens import account_activation_token


def login_propio(request):
    template = join("default", "usuarios", "login.html")
    form = AuthenticationForm()
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = AuthenticationForm(data=request.POST)
        # Si el formulario es válido...
        if form.is_valid():
            # Recuperamos las credenciales validadas
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            # Verificamos las credenciales del usuario
            user = authenticate(username=username, password=password)

            # Si existe un usuario con ese nombre y contraseña
            if user is not None:
                do_login(request, user)
                return redirect('/')
            else:
                messages.error(request, "Contraseña o usuario no valido. Verifique!!!")
        # else:
        #     print('{}'.format(form.errors))

    # Si llegamos al final renderizamos el formulario
    return render(request, template, {'form': form})

def user_logout(request):
    logout(request)
    return redirect('/')

def registrate(request):
    template = join("fasa", "usuarios", "register.html")
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()

            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            # user = authenticate(username=username, password=raw_password)
            # user.refresh_from_db()  # load the profile instance created by the signal
            user.perfil.celular = form.cleaned_data.get('celular')
            user.save()

            # login(request, user)
            # return redirect('inicio:inicio')
            current_site = get_current_site(request)
            subject = 'Activa tu usuario'
            message = render_to_string('fasa/usuarios/account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            user.email_user(subject, message)

            return redirect('usuarios:account_activation_sent')
    else:
        form = SignUpForm()

    return render(request, template_name=template, context={
        'form':form
    })

def account_activation_sent(request):
    template = join("fasa", "usuarios", "account_activation_sent.html")
    return render(request, template_name=template)


def activate(request, uidb64, token):
    template = join("fasa", "usuarios", "account_activation_invalid.html")
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        # user.profile.email_confirmed = True
        user.save()
        login(request, user)
        return redirect('inicio:inicio')
    else:
        return render(request, template_name=template)